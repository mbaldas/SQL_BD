CREATE TABLE usuario(
	id_usuario INTEGER PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR (20)
);

CREATE TABLE post(
  id_post INTEGER PRIMARY KEY AUTO_INCREMENT,
  date_post DATE,
  conteudo_post VARCHAR (255)
);

CREATE TABLE comentario(
	id_com INTEGER PRIMARY KEY AUTO_INCREMENT,
	date_com DATE,
	conteudo_com VARCHAR (30)
);


INSERT INTO post (date_post, conteudo_post)
VALUES 	("2018-02-11","Primeiro post"),
	 	("2018-04-11","Segundo post"),
		("2018-08-11","Terceiro post");

INSERT INTO comentario (date_com, conteudo_com)
VALUES 	("2018-02-11","Testando"),
		("2018-02-11","Testando"),
 		("2018-02-11","Testing");

INSERT INTO usuario (nome)
VALUES 	("Baldo"),
		("Baldas"),
 		("Baldinhas");
